import sys
import numpy as np
import matplotlib.pyplot as plt

from scipy import fftpack, signal
from tqdm import tqdm



def main():

    if len(sys.argv) == 1:
        ACE='datafiles/ACE_data_Filtered_Data_calculated_data.txt'
        lpf='datafiles/g2_alldat_neg1001_Filtered_Data.txt'
        graphSaveFile='plots/ACE_and_LPF_coherence'
    elif len(sys.argv) == 2 and sys.argv[2] == "test":
        ACE='datafiles/modified_ACE_data.txt'
        lpf='datafiles/modified_LPF_data.txt'
        graphSaveFile='plots/ACE_and_LPF_coherence_test'
    elif len(sys.argv) == 3 and sys.argv[2] == "test":
        ACE='datafiles/modified_ACE_data.txt'
        lpf='datafiles/modified_LPF_data.txt'
        graphSaveFile=sys.argv[3]
    else:
        sys.stderr.write(f'usage: {sys.argv[0]} test \{plot save file\}\n')


    lpftimes='datafiles/LISA_full_25_gf.txt'
    acetimes='datafiles/ACE_time_seconds.txt'
    aced=np.loadtxt(fname=ACE, usecols=range(3))
    lpfd=np.loadtxt(fname=lpf)
    lpft=np.loadtxt(fname=lpftimes, usecols=range(2))
    acetimesd=np.loadtxt(fname=acetimes)
    a=np.array(aced[0:, 1])
    l=np.array(lpfd)
    lt=np.array(lpft[0:, 0])
    at=np.array(acetimesd)

    for i in range(at.size):
        if at[i]>lt[0]-1:
            at=at[i-1:]
            a=a[i-1:]
            break
    for i in range(at.size):
        if at[i] > lt[-1] + 1:
            at=at[:i]
            a=a[:i]
            break
    a=np.interp(lt, at, a)
    plt.rcParams["figure.figsize"] = (16,11)
    mpl.rcParams['agg.path.chunksize'] = 500000000000000000
    plt.rcParams['agg.path.chunksize'] = 500000000000000000
    plt.rcParams.update({'font.size': 22})

    fs=2*1/(16384/(3276*2))
    nn=lt.size//2
    print(nn)
    plt.title("Coherence of ACE and lpf")
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Coherence")
    plt.grid()
    #
    for i in [1,3,5]:
        nn=lt.size//2
        nn=nn//(2**(i*2))
        nn=int(nn)
        #print(nn, i)
        f, Cxy = signal.coherence(a, l, fs, nperseg=int(nn))
        #print(f.size, Cxy.size)
        #print(Cxy)
        #print("info stuff")
        #print("f", f)
        #print("Cxy", Cxy)
        plt.loglog(f, Cxy, label="N = "+str(2**(i*2)))
        #plt.title("Coherence of ACE and LPF with Injected Excitations at 5e-3")
        plt.xlabel("Frequency (Hz)")
        plt.ylabel("Coherence")
        plt.grid()

    #nn=lt.size//2
    #nn=nn//(2**(5*2))
    np.set_printoptions(threshold=sys.maxsize)

    print("info stuff")
    print("f", f)
    print("Cxy", Cxy)
    #f, Cxy = signal.coherence(a, l, fs, nperseg=nn)
    #plt.loglog(f, Cxy, label="N = "+str(2**(5*2)))
    plt.axvline(x=5e-3, color='r', linestyle='--', label="5e-3 Hz", alpha=0.3)
    plt.axvline(x=7.8125e-3, color='r', linestyle='-', label="7.8125e-3 Hz", alpha=0.3)
    #plt.ylabel('Coherence')
    #plt.title("Coherence of ACE and LPF with Injected Excitations at 5e-3")
    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Coherence")
    plt.grid()
    plt.legend(loc="lower left")
    ftypes=['png']
    saveplot(graphSaveFile, ftypes)
    plt.show()


def saveplot(title, filetypes):
    for ftype in filetypes:
        filename=f'{title}.{ftype}'
        print(f'saving file {filename}')
        plt.savefig(filename)

main()





