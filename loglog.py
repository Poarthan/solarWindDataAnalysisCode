import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from scipy import fftpack, signal

def main():
    global aces, big, at, lt, l, lpfs, a, freqace, freqlpf, freqlpf2, lpfs2
    ACE='datafiles/ACE_data_Filtered_Data_calculated_data.txt'
    lpf='datafiles/g2_alldat_neg1001_Filtered_Data.txt'
    lpf2='datafiles/LISA_full_25_gf.txt'
    acetimes='datafiles/ACE_time_seconds.txt'
    aced=np.loadtxt(fname=ACE, usecols=range(3))
    lpfd=np.loadtxt(fname=lpf)
    lpft=np.loadtxt(fname=lpf2, usecols=range(2))
    acetimesd=np.loadtxt(fname=acetimes)
    a=np.array(aced[0:, 0])
    l=np.array(lpfd)
    lt=np.array(lpft[0:, 0])
    at=np.array(acetimesd)
    for i in range(at.size):
        if at[i]>lt[0]-1:
            at=at[i-1:]
            a=a[i-1:]
            break
    for i in range(at.size):
        if at[i] > lt[-1] + 1:
            at=at[:i]
            a=a[:i]
            break
    #a=np.interp(lt, at, a)
    ace=np.fft.fft(a)
    lpf=np.fft.fft(l)
    time_step_lpf= (16384/3276)/2
    time_step_ace = 64
    freqlpf = np.fft.fftfreq(len(lt), d=time_step_lpf)
    freqace = np.fft.fftfreq(len(at), d=time_step_ace)
    plt.rcParams["figure.figsize"] = (16,11)
    plt.rcParams.update({'font.size': 20})
    plt.rc('font', size=20)          # controls default text sizes
    plt.rc('axes', titlesize=30)     # fontsize of the axes title
    plt.rc('axes', labelsize=30)    # fontsize of the x and y labels
    plt.rc('legend', fontsize=25)    # legend fontsize
    plt.rc('figure', titlesize=100)  # fontsize of the figure title

    prelpf = (lt[1]-lt[0])*np.sqrt(freqlpf[1]-freqlpf[0])
    preace = (at[1]-at[0])*np.sqrt(freqace[1]-freqace[0])


    #print("prelpf", prelpf)
    #print("dt", lt[1]-lt[0])
    #print("dt", time_step_lpf)
    #print("df", freqlpf[1]-freqlpf[0])

    #print("preace", preace)
    #print("dt", at[1]-at[0])

    plotter(ace, lpf, freqace, freqlpf, preace,prelpf)

    # interp plot:
    #plotter(ace, lpf, freqlpf, freqlpf, preace,prelpf)
    #saving plot
    #ftypes=['jpg']
    ftypes=['png']
    saveplot(f'plots/spectralDensity', ftypes)

    plt.show()

def plotter(ace, lpf, freqace, freqlpf, preace,prelpf):
    plt.title('ACE and LPF FFT Comparison')

    #plt.loglog(np.abs(freqace),np.abs(ace), np.abs(freqlpf), np.abs(lpf), alpha=0.5)
    plt.loglog(np.abs(freqace),np.abs(ace)*preace, np.abs(freqlpf), np.abs(lpf)*prelpf, alpha=0.5)

    blue_patch = mpatches.Patch(color='blue', label='ACE')
    orange_patch = mpatches.Patch(color='orange', label='LPF')

    plt.legend(handles=[blue_patch, orange_patch])
    plt.xlabel('sqrt(Hz)')
    plt.ylabel('Force (N)')
    plt.grid(True)


def saveplot(title, filetypes):
    for ftype in filetypes:
        filename=f'{title}.{ftype}'
        print(f'saving file {filename}')
        plt.savefig(filename)

main()
