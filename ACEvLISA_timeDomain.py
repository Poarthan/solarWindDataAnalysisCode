import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from scipy import fftpack
from tqdm import tqdm

def main():
    ACE='datafiles/ACE_data_Filtered_Data_calculated_data.txt'
    lisa='datafiles/g2_alldat_neg1001_Filtered_Data.txt'
    lisa2='datafiles/LISA_full_25_gf.txt'
    acetimes='datafiles/ACE_time_seconds.txt'
    aced=np.loadtxt(fname=ACE, usecols=range(3))
    lisad=np.loadtxt(fname=lisa)
    lisat=np.loadtxt(fname=lisa2, usecols=range(2))
    acetimesd=np.loadtxt(fname=acetimes)
    a=np.array(aced[0:, 1])##*2*10**12
    l=np.array(lisad)
    lt=np.array(lisat[0:, 0])
    at=np.array(acetimesd)

    # gaps
    gaps_LPF = np.loadtxt("datafiles/gap_LPF.txt")  # Gaps for axis[0]

    for i in tqdm(range(at.size)):
        if at[i]>lt[0]-1:
            at=at[i-1:]
            a=a[i-1:]
            break
    for i in tqdm(range(at.size)):
        if at[i] > lt[-1] + 1:
            at=at[:i]
            a=a[:i]
            break

    plt.rcParams["figure.figsize"] = (16,11)
    plt.rcParams['agg.path.chunksize'] = 500000000000000000
    plt.rcParams.update({'font.size': 22})
    plt.figure(dpi=600)


    plot_(a, l, at, lt, gaps_LPF)

    #saving plot
    ftypes=['png']
    saveplot(f'plots/TimeDomain_Comparison_ACEvLPF', ftypes)

    plt.show()


def plot_(aa, ll, ati, lti, gaps_LPF):
    figure, axis = plt.subplots(2,1)
    #axis[0].plot(array)
    #axis[0].set_title(f"ACE & LPF Data")
    axis[0].set_title(f"LPF Data")
    axis[1].set_title(f"ACE Data")

    #scale/unit of signal of time is currently unknown

    plt.setp(axis[0], xlabel=" ")
    plt.setp(axis[0], ylabel=f"Force(N)")
    axis[0].grid()
    plt.setp(axis[1], xlabel="Time (GPS seconds)")
    plt.setp(axis[1], ylabel=f"Force(N)")
    axis[1].grid()
    plt.grid()

    for start, end in gaps_LPF:
        axis[0].axvspan(start, end, color='gray', alpha=0.2, label='Gaps', zorder=20)

    blue_patch = mpatches.Patch(color='blue', label='ACE')
    orange_patch = mpatches.Patch(color='orange', label='LPF')
    gap_patch = mpatches.Patch(color='gray', alpha=0.2, label='Gaps')
    plt.legend(handles=[gap_patch], loc='lower right')
    axis[0].plot(lti, ll, label='LPF')
    axis[1].plot(ati, aa, color='orange', label='ACE')
    axis[1].grid()

def saveplot(title, filetypes):
    for ftype in filetypes:
        filename=f'{title}.{ftype}'
        print(f'saving file {filename}')
        plt.savefig(filename)

def plot_original(times, sig, subp, ylab):
    plt.subplot(subp)
    plt.ylabel(ylab)
    #not the best way to make title and labels, will improve later
    #plt.title(ylab+"                           ", fontsize=13, ha="right")
    #plt.ylabel("signal")
    #plt.xlabel("frequency")
    plt.plot(times, sig, label=ylab)

def plot_fft(freqs, sigfft, subp, ylab):
    plt.subplot(subp)
    plt.ylabel(ylab)
    #not the best way to make the title and labels, will improve later
    #plt.title(ylab+"                           ", fontsize=13, ha="right")
    #plt.ylabel("signal strength")
    #plt.xlabel("frequency")
    markerline, stemlines, baseline = plt.stem(freqs, np.abs(sigfft), '-.')
    plt.setp(stemlines, 'linewidth', 1)
    np.semilogy(np.abs(freqs), np.abs(sigfft))
# plt.stem(freqs, np.abs(sigfft))


main()
