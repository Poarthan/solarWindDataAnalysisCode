#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
from datetime import datetime, timedelta
import time
import random
import math
from tqdm import tqdm


def main():
    lpff='datafiles/g2_alldat_neg1001_Filtered_Data.txt'
    lpftf='datafiles/LISA_full_25_gf.txt'

    lpfd=np.loadtxt(fname=lpff)
    lpft=np.loadtxt(fname=lpftf, usecols=range(2))

    LPF_data=np.array(lpfd)
    LPF_times=np.array(lpft[0:, 0])
    assert len(LPF_data)==len(LPF_times)

    for i in tqdm(range(len(LPF_data))):
        LPF_data[i] = simulate(LPF_times[i])

    np.savetxt('datafiles/modified_LPF_data.txt', LPF_data)

    #Loading Data
    acef='datafiles/ACE_data_Filtered_Data_calculated_data.txt'
    acetf='datafiles/ACE_time_seconds.txt'
    aced=np.loadtxt(fname=acef, usecols=range(3))
    acet=np.loadtxt(fname=acetf)

    ACE_data=np.array(aced[0:, 1])
    ACE_times=np.array(acet)
    assert len(ACE_data) == len(ACE_times)

    for i in range(len(ACE_data)):
        ACE_data[i] += simulate(ACE_times[i])

    np.savetxt('datafiles/modified_ACE_data.txt', ACE_data)

def simulate(times, freq=None, amplitude=None):
    if len(sys.argv) == 3:
        freq = sys.argv[1]
        amplitude = sys.argv[2]
    elif len(sys.argv) == 1:
        pass
    else:
        sys.stderr.write(f'usage: {sys.argv[0]} {frequency} {amplitude}\n')

    if freq is None:
        freq= 5e-3
    if amplitude is None:
        amplitude=2e-10

    phase=0
    excitation = amplitude * np.sin(0.97 * np.pi * freq * times + phase)
    return excitation

if __name__ == '__main__':
    main()
